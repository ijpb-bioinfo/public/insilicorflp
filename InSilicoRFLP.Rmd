---
title: "Rmarkdown to perform in silico RFLP analysis"
author: "Delphine Charif and Françoise Budard"
date: "2019-11"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Context and goal:

This Rmarkdown performs in silico RFLP. It has been used to validate the structure of Arabidopsis thaliana Shadhara and Kz-9 mitochondrial genomes assemblies (ref).
It uses the seqRFLP R package and simulate the digestion of a genome assembly by several restriction enzymes at a time. Output fragments are those matching with the probes.
WARNING: This package has been removed from CRAN since 2022 but the source version is still available at https://cran.r-project.org/src/contrib/Archive/seqRFLP/seqRFLP_1.0.1.tar.gz.

Input:
  * assemblyFasta:  genome assembly in fasta format (one or several sequences)
  * enzList: a vector of restriction enzymes names
  * probesList: a vector of probes names 
  * blastFile: blast output (with -outfmt 7 tabular BLAST format) of  probes matches against the genome assembly.
  * resultFile: the name of the output file

Output:
  * A data frame with 6 columns:
    * Seq: sequence's name or ID
    * Enz: the name of the restriction enzyme
    * Frag: the length of the digested fragment 
    * isSondMatch: a comma separated list of probes that match with the fragment
    * isStart: is it the first fragment of fasta sequence
    * isEnd: is it the last fragment of fasta sequence
  

# Get the seqRFLP package sources

```{bash, eval=FALSE}
wget https://cran.r-project.org/src/contrib/Archive/seqRFLP/seqRFLP_1.0.1.tar.gz
gunzip seqRFLP_1.0.1.tar.gz
tar -xvf seqRFLP_1.0.1.tar
```


# Load package and source seqRFLP functions and data

```{r load}
source("seqRFLP/R/enzCut.R")
source("seqRFLP/R/read.fasta.R")
source("seqRFLP/R/ConvFas.R")
source("seqRFLP/R/selEnz.R")
source("seqRFLP/R/revComp.R")
load("seqRFLP/data/enzdata.rda")
library(stringr)
```

# Set input and output parameters:

```{r parameters}

# input files
assemblyFasta <- "input/AtShaMt.fasta"
blastFile <- "input/blast_results_AtShaMt_10.txt"
#blastFile <- "input/blast_results_AtShaMt_7.txt"

# output files
resultFile <- "output/shacor10.txt"
#resultFile <- "output/shacor7.txt"

# List of enzymes
enzList <- c("BamHI", "EcoRI", "HindIII", "PstI", "PvuII", "SpeI", "XbaI")
#enzList <-c("HindIII","NcoI","NruI","PvuII","SpeI","StuI","XhoI")

# List of probes
probesList <- "ccb203"
#probesList <- c("atp9","ccb203","cox3","rpl5","rps7")


# Check that the restriction enzymes are in the enzdata dataset
if (length(enzList[!enzList %in% enzdata$nam]) > 0) {
  stop(paste("The following enzymes are not in the enzdata:", enzList[!enzList %in% enzdata$nam],"\n"))
}
```


# Load input data:

```{r input}

# Import fasta sequences
seq1 <- read.fasta(assemblyFasta)

# Import Blast results
blastResult <- read.table(blastFile, h = FALSE, colClasses = "vector")
names(blastResult) <-
  c("query",
    "subject",
    "Identity",
    "alignment_length",
    "mismatches",
    "gap_opens",
    "q.start",
    "q.end",
    "s.start",
    "s.end",
    "evalue",
    "bit_score"
  )

# utils
nb_Seq <- length(seq1) / 2
Seq <- seq1[seq(2, nb_Seq * 2, by = 2)]
Names <- str_replace(seq1[seq(1, nb_Seq * 2, by = 2)], ">", "")
sondeNumber <- c(1:length(probesList))

```


# Digest each sequences by each enzymes and highlight fragments that matches with the probes


```{r rflp}

# Create an integer vector of 0 corresponding to the length of the fasta sequences: 
# put 1 to the positions which match with the sondes

ListSeq <- lapply(Seq, function(x)
  rep(0, nchar(x)))
names(ListSeq) <- Names


 i<-1
  while(i <= dim(blastResult)[1]){
    subject<-blastResult[i,"subject"]
    while(subject==blastResult[i,"subject"] & i<= dim(blastResult)[1]){
      ListSeq[[subject]][blastResult[i,"s.start"]:blastResult[i,"s.end"]] <- sondeNumber[which(probesList %in% blastResult[i,"query"])] 
      i<-i+1
    }
  }
 

CutList <- list()
iNames = ""

# For each sequence
for (iNames in Names) {
  # For each enzyme
  for (j in 1:length(enzList)) {
    # Obtain the fragments lists
    CutList[[iNames]][[j]] <-
      enzCut(DNAsq = Seq[Names == iNames],
             enznames = enzList[j],
             enzdata = enzdata)
    
    # digest the integer vector and create a list of integer vector
    
     listSeq <-list()
  if(length(CutList[[iNames]][[j]]$RFLP.site) ==0){
    lFrag <- c(CutList[[iNames]][[j]]$RFLP.site+1,l=length(ListSeq[[iNames]]))
  } 
  else{
    lFrag <- c(1,CutList[[iNames]][[j]]$RFLP.site,l=length(ListSeq[[iNames]]))
  }
    for(k in 1:(length(lFrag)-1)){
        listSeq[[k]]<-ListSeq[[iNames]][lFrag[k]:lFrag[k+1]]
    }
    
    CutList[[iNames]][[j]]$RFLP.seqInt <- listSeq
    
    # Indicate if the fragment is at the begining or the end of the sequence
    CutList[[iNames]][[j]]$isStartSeq <-
      replace(integer(length(listSeq)), 1, 1)
    CutList[[iNames]][[j]]$isEndSeq <-
      replace(integer(length(listSeq)), length(listSeq), 1)
    
    # Add sequence name
    CutList[[iNames]][[j]]$SeqNames <- iNames
    
    # For each sequences and enzymes, create a vector to indicate the probes that matchs with fragments
   CutList[[iNames]][[j]]$RFLP.match.probes  <- 
     unlist(lapply(CutList[[iNames]][[j]]$RFLP.seqInt,
                  function(x) {
                    paste(probesList[as.numeric(names(table(x)))], collapse = ",")
                  }))
  }
}

```

# Convert the list of results to data.frame and export results in a txt file:

```{r export}

CutList2df <- function(CutList) {
  listOfDataFrames <- lapply(CutList, function(x) {
    data.frame(
      "Seq" = rep(x$SeqNames, length(x$RFLP.frag)),
      "Enz" = rep(x$enz$nam, length(x$RFLP.frag)),
      "Frag" = x$RFLP.frag,
      "ProbesMatch" = x$RFLP.match.probes,
      "isStart" = x$isStartSeq,
      "isEnd" = x$isEndSeq
    )
  })
  do.call("rbind", listOfDataFrames)
}

CutListdf <- do.call("rbind", lapply(CutList,CutList2df))

CutListdf <-CutListdf[which(CutListdf$ProbesMatch != ""),]

write.table(CutListdf,file= resultFile ,row.names = F,quote=F,sep="\t")
```

```{r}
sessionInfo()
```









