# insilicorflp


This Rmarkdown performs in silico RFLP. It has been used to validate the structure of Arabidopsis thaliana Shadhara and Kz-9 mitochondrial genomes assemblies (ref).
It uses the seqRFLP R package and simulate the digestion of a genome assembly by several restriction enzymes at a time. Output fragments are those matching with the probes.

## Dependencies

- [R](https://www.r-project.org/) (>= 4.0.0)
- seqRFLP R package: https://cran.r-project.org/src/contrib/Archive/seqRFLP/seqRFLP_1.0.1.tar.gz


## Get the seqRFLP package sources

```bash
wget https://cran.r-project.org/src/contrib/Archive/seqRFLP/seqRFLP_1.0.1.tar.gz
gunzip seqRFLP_1.0.1.tar.gz
tar -xvf seqRFLP_1.0.1.tar
```
### Input:
  * assemblyFasta:  genome assembly in fasta format (one or several sequences)
  * enzList: a vector of restriction enzymes names
  * probesList: a vector of probes names 
  * blastFile: blast output (with -outfmt 7 tabular BLAST format) of  probes matches against the genome assembly.
  * resultFile: the name of the output file

### Output:
  * A txt file with 6 columns:
      * Seq: sequence's name or ID
      * Enz: the name of the restriction enzyme
      * Frag: the length of the digested fragment 
      * isSondMatch: a comma separated list of probes that match with the fragment
      * isStart: is it the first fragment of fasta sequence
      * isEnd: is it the last fragment of fasta sequence

## Usage
  
  edit InsilicoRFLP.Rmd, change input/output parameters and run it with Rstudio 

## Authors 

Delphine Charif and Françoise Budard

## License

[![GPL-3.0-or-later](assets/img/gplv3-or-later.png)](ttps://www.gnu.org/licenses/gpl-3.0.fr.html)  
see `COPYING`
